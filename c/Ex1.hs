module Ex1 where

-- Exercício String-o Permute
-- https://www.hackerrank.com/challenges/string-o-permute/problem

import Control.Monad

solve :: String -> String
solve [] = []
solve (c1 : c2 : xs) = c2 : c1 : solve xs

-- toda String de input deve ser par

main :: IO ()
main = do
  n <- readLn
  replicateM_
    n
    ( do
        str <- getLine
        putStrLn (solve str)
    )
