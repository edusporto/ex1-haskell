module Ex2 where

-- Exercício String Mingling
-- https://www.hackerrank.com/challenges/string-mingling/problem

solve :: String -> String -> String
solve s1 s2 = concat (zipWith (\c1 c2 -> [c1, c2]) s1 s2)

main :: IO ()
main = do
  s1 <- getLine
  s2 <- getLine
  putStrLn (solve s1 s2)
