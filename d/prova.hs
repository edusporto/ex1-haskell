
-- Queremos provar:
-- 3 * a \equiv a + a + a

data Either3 a b c = Left3 a | Middle3 b | Right3 c

-- (3, a) \equiv Either3 a a a

data Op3 = A | B | C

-- (A | B | C, a) \equiv Either3 a a a

ida :: (Op3, a) -> Either3 a a a
ida (arg, a) =
  case arg of
    A -> Left3 a
    B -> Middle3 a
    C -> Right3 a

volta :: Either3 a a a -> (Op3, a)
volta arg =
  case arg of
    Left3 a -> (A, a)
    Middle3 a -> (B, a)
    Right3 a -> (C, a)

-- agora, demonstrar que (ida . volta) e (volta . ida) são a função identidade

{-

ida (volta arg) = arg

Caso 1: arg = Left3 a
ida (volta Left3 a) = ida (A, a)
ida (A, a) = Left3 a
\therefore ida (volta Left3 a) = Left3 a

Caso 2: arg = Middle3 a
ida (volta Middle3 a) = ida (B, a)
ida (B, a) = Middle3 a
\therefore ida (volta Middle3 a) = Middle3 a

Caso 3: arg = Right3 a
ida (volta Right3 a) = ida (C, a)
ida (C, a) = Right3 a
\therefore ida (volta Right3 a) = Right3 a

\therefore ida (volta arg) = arg


volta (ida arg) = arg

Caso 1: arg = (A, a)
volta (ida (A, a)) = volta Left3 a
volta Left3 a = (A, a)
\therefore volta (ida (A, a)) = (A, a)

Caso 2: arg = (B, a)
volta (ida (B, a)) = volta Middle3 a
volta Middle3 a = (B, a)
\therefore volta (ida (B, a)) = (B, a)

Caso 3: arg = (C, a)
volta (ida (C, a)) = volta Right3 a
volta Right3 a = (C, a)
\therefore volta (ida (C, a)) = (C, a)

\therefore volta (ida arg) = arg

-}

-- assim, `(A | B | C, a)` e `Either3 a a a` são estruturas isomórficas
-- por consequência, `3 * a \equiv a + a + a`                    Q.E.D.
