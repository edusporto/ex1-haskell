module Dia2_2 where

-- Segundo dia do Advent of Code 2020
-- Parte 2

-- Para ler o arquivo, usei a seguinte resposta do Stackoverflow:
-- https://stackoverflow.com/questions/22547430/haskell-readfile-line-by-line-and-put-into-list

import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Definitions

verificaValido :: Senha -> Bool
verificaValido s
  | a && not b = True
  | not a && b = True
  | otherwise = False
  where
    a = senha s !! (minC s - 1) == letra s
    b = senha s !! (maxC s - 1) == letra s

main :: IO ()
main = do
  lines <- fmap Text.lines (Text.readFile "input_Dia2.txt")
  let senhas = map (constroiSenha . Text.unpack) lines

  print (length $ filter verificaValido senhas)
