module Definitions where

data Senha = Senha
  { minC :: Int,
    maxC :: Int,
    letra :: Char,
    senha :: [Char]
  }
  deriving (Show)

ehNumero :: Char -> Bool
ehNumero c = ('0' <= c) && (c <= '9')

ehLetra :: Char -> Bool
ehLetra l = (('a' <= l) && (l <= 'z')) || (('A' <= l) && (l <= 'Z'))

-- Troca caracteres nao desejados por espacos
caracteresPorEspacos :: [Char] -> [Char]
caracteresPorEspacos [] = []
caracteresPorEspacos (x : xs)
  | ehLetra x || ehNumero x = x : caracteresPorEspacos xs
  | otherwise = ' ' : caracteresPorEspacos xs

constroiSenha :: [Char] -> Senha
constroiSenha xs =
  let [minC, maxC, letra, senha] = words (caracteresPorEspacos xs)
   in Senha {minC = read minC, maxC = read maxC, letra = head letra, senha = senha}
