module Dia4_1 where

-- Quarto dia do Advent of Code 2020
-- Parte 1

-- Para ler o arquivo, usei a seguinte resposta do Stackoverflow:
-- https://stackoverflow.com/questions/22547430/haskell-readfile-line-by-line-and-put-into-list

import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Definitions

juntar :: String -> String -> String
juntar s1 "" = s1 ++ ['\n']
juntar "" s2 = '\n' : s2
juntar s1 s2 = s1 ++ " " ++ s2

juntarLinhas :: [String] -> String
juntarLinhas = foldl juntar ""

verificaPassaporte :: Passaporte -> Bool
verificaPassaporte p =
  case p of
    Passaporte
      (Just _)
      (Just _)
      (Just _)
      (Just _)
      (Just _)
      (Just _)
      (Just _)
      _ ->
        -- `cid` não será utilizado
        True
    _ -> False

main :: IO ()
main = do
  lines <- fmap Text.lines (Text.readFile "input_Dia4.txt")
  let stringsPassaporte = split ('\n' ==) (juntarLinhas $ map Text.unpack lines)
  let passaportes = map constroiPassaporte stringsPassaporte

  print (length $ filter verificaPassaporte passaportes)
