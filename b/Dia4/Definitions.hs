module Definitions where

data Passaporte = Passaporte
  { byr :: Maybe String,
    iyr :: Maybe String,
    eyr :: Maybe String,
    hgt :: Maybe String,
    hcl :: Maybe String,
    ecl :: Maybe String,
    pid :: Maybe String,
    cid :: Maybe String
  }
  deriving (Show)

-- Funcao `split` adaptada de
-- https://stackoverflow.com/questions/4978578/how-to-split-a-string-in-haskell

split :: (Char -> Bool) -> String -> [String]
split p s = case dropWhile p s of
  "" -> []
  s' -> w : split p s''
    where
      (w, s'') = break p s'

chaveValor :: String -> (String, String)
chaveValor str = (head s, tail (head s))
  where
    s = split (':' ==) str

passaporteVazio :: Passaporte
passaporteVazio =
  Passaporte
    { byr = Nothing,
      iyr = Nothing,
      eyr = Nothing,
      hgt = Nothing,
      hcl = Nothing,
      ecl = Nothing,
      pid = Nothing,
      cid = Nothing
    }

adicionaAoPassaporte :: (String, String) -> Passaporte -> Passaporte
adicionaAoPassaporte ("byr", val) p = p {byr = Just val}
adicionaAoPassaporte ("iyr", val) p = p {iyr = Just val}
adicionaAoPassaporte ("eyr", val) p = p {eyr = Just val}
adicionaAoPassaporte ("hgt", val) p = p {hgt = Just val}
adicionaAoPassaporte ("hcl", val) p = p {hcl = Just val}
adicionaAoPassaporte ("ecl", val) p = p {ecl = Just val}
adicionaAoPassaporte ("pid", val) p = p {pid = Just val}
adicionaAoPassaporte ("cid", val) p = p {cid = Just val}

-- Constrói um passaporte através de uma lista de listas chave:valor
processaPassaporte :: [(String, String)] -> Passaporte
processaPassaporte = foldr adicionaAoPassaporte passaporteVazio

constroiPassaporte :: String -> Passaporte
constroiPassaporte xs = processaPassaporte $ map chaveValor (words xs)
