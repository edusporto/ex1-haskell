module Exs where

-- Exercício 2.4
tamanho :: [[Char]] -> [Int]
tamanho = map length

-- Exercício 2.3
reversa :: [[Char]] -> [[Char]]
reversa = map reverse

-- Exercício 3.1
data Pergunta = Sim | Nao deriving(Show, Eq, Enum)

pergNum :: Pergunta -> Int
pergNum p
    | p == Nao = 0
    | p == Sim = 1

listPergs :: [Pergunta] -> [Int]
listPergs = map pergNum

and' :: Pergunta -> Pergunta -> Pergunta
and' Nao Nao = Nao
and' Nao Sim = Nao
and' Sim Nao = Nao
and' Sim Sim = Sim

or' :: Pergunta -> Pergunta -> Pergunta
or' Nao Nao = Nao
or' Nao Sim = Sim
or' Sim Nao = Sim
or' Sim Sim = Sim

not' :: Pergunta -> Pergunta
not' Nao = Sim
not' Sim = Nao

-- Exercício 3.3
data PedraPapelTesoura = Pedra | Papel | Tesoura
    deriving(Show, Eq, Enum)

vencedor :: PedraPapelTesoura -> PedraPapelTesoura -> Maybe PedraPapelTesoura
vencedor Pedra Tesoura = Just Pedra
vencedor Tesoura Pedra = Just Pedra
vencedor Pedra Papel = Just Papel
vencedor Papel Pedra = Just Papel
vencedor Papel Tesoura = Just Tesoura
vencedor Tesoura Papel = Just Tesoura
vencedor _ _ = Nothing
